build:
	docker build . -t shimun/nginx-exposed

push: build
	docker push shimun/nginx-exposed
